package com.example.conferenceroombooking.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy= AvailabilityValidator.class)
@Target(ElementType.TYPE)
@Retention(RUNTIME)
public @interface AvailabilityAnnotation {
    String message() default "Conference room is not available at given time";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
