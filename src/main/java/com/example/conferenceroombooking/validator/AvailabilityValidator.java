package com.example.conferenceroombooking.validator;

import com.example.conferenceroombooking.dao.BookingService;
import com.example.conferenceroombooking.model.Booking;
import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

@Component
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class AvailabilityValidator implements ConstraintValidator<AvailabilityAnnotation, Booking> {
    @Autowired
    BookingService bookingService;

    @Override
    public void initialize(AvailabilityAnnotation constraintAnnotation) {
        
    }

    @Override
    public boolean isValid(Booking object, ConstraintValidatorContext context) {
        try {
            List<Booking> bookings = bookingService.getBookings();
            for (Booking booking : bookings) {
                if (booking.getConferenceRoom().getName().equals(object.getConferenceRoom().getName()) && booking.getFinish().isAfter(object.getStart()) && booking.getStart().isBefore(object.getFinish())) {
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }
}
