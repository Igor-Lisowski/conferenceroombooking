package com.example.conferenceroombooking.serializer;

import com.example.conferenceroombooking.model.Booking;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookingSerializer extends StdSerializer<List<Booking>> {
    public BookingSerializer() {
        this(null);
    }

    public BookingSerializer(Class<List<Booking>> t) {
        super(t);
    }

    @Override
    public void serialize(List<Booking> bookings, JsonGenerator generator, SerializerProvider provider) throws IOException {
        List<Booking> bs = new ArrayList<>();
        for (Booking b : bookings) {
            b.getOrganization().setBookings(null);
            b.getConferenceRoom().setBookings(null);
            bs.add(b);
        }
        generator.writeObject(bs);
    }

}
