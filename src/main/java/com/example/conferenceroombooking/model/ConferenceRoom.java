package com.example.conferenceroombooking.model;

import com.example.conferenceroombooking.serializer.BookingSerializer;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
@Table(name = "ConferenceRoom")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ConferenceRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name", unique = true)
    @NotNull(message = "Please provide name")
    @Size(min = 2, max = 20, message = "Name must be between 2 and 20 characters long")
    @Pattern(regexp = ".*\\S.*", message = "Name must contain at least one non-whitespace character")
    private String name;
    @Column(name = "floor")
    @NotNull(message = "Please provide floor")
    @Min(0)
    @Max(10)
    private int floor;
    @Column(name = "isAvailable")
    @NotNull(message = "Please provide isAvailable")
    private boolean isAvailable;
    @Column(name = "seatsNumber")
    @NotNull(message = "Please provide seatsNumber")
    @Min(0)
    private int seatsNumber;
    @Column(name = "standingPlacesNumber")
    @NotNull(message = "Please provide standingPlacesNumber")
    @Min(0)
    private int standingPlacesNumber;
    @Column(name = "lyingPlacesNumber")
    private int lyingPlacesNumber;
    @Column(name = "hangingPlacesNumber")
    private int hangingPlacesNumber;
    @Column(name = "projectorName")
    private String projectorName;
    @Column(name = "isTelephone")
    private boolean isTelephone;
    @Column(name = "extensionNumber")
    @Min(0)
    @Max(100)
    private int extensionNumber;
    @Column(name = "externalNumber")
    private int externalNumber;
    @Column(name = "connection")
    @Pattern(regexp = "^bluetooth$|^USB$", message = "Please provide bluetooth or USB")
    private String connection;
    @JsonSerialize(using = BookingSerializer.class)
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy="conferenceRoom")
    private List<Booking> bookings;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public int getStandingPlacesNumber() {
        return standingPlacesNumber;
    }

    public void setStandingPlacesNumber(int standingPlacesNumber) {
        this.standingPlacesNumber = standingPlacesNumber;
    }

    public int getLyingPlacesNumber() {
        return lyingPlacesNumber;
    }

    public void setLyingPlacesNumber(int lyingPlacesNumber) {
        this.lyingPlacesNumber = lyingPlacesNumber;
    }

    public int getHangingPlacesNumber() {
        return hangingPlacesNumber;
    }

    public void setHangingPlacesNumber(int hangingPlacesNumber) {
        this.hangingPlacesNumber = hangingPlacesNumber;
    }

    public String getProjectorName() {
        return projectorName;
    }

    public void setProjectorName(String projectorName) {
        this.projectorName = projectorName;
    }

    public boolean isTelephone() {
        return isTelephone;
    }

    public void setTelephone(boolean telephone) {
        isTelephone = telephone;
    }

    public int getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(int extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public int getExternalNumber() {
        return externalNumber;
    }

    public void setExternalNumber(int externalNumber) {
        this.externalNumber = externalNumber;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
