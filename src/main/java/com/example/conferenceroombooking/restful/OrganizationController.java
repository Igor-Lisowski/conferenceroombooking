package com.example.conferenceroombooking.restful;

import com.example.conferenceroombooking.dao.OrganizationService;
import com.example.conferenceroombooking.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class OrganizationController {
    @Autowired
    OrganizationService organizationService;

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(path="/organization")
    ResponseEntity<String> addOrganization(@Valid @RequestBody Organization organization) {
        organizationService.saveOrganization(organization);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(path="/organization")
    Iterable<Organization> getOrganizations() {
        return organizationService.getOrganizations();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(path="/organization/{id}")
    Organization getOrganizationById(@PathVariable Long id) {
        return organizationService.getOrganizationById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping(path="/organization/{id}")
    ResponseEntity<String> updateOrganization(@Valid @RequestBody Organization organization, @PathVariable Long id) {
        organizationService.updateOrganizationById(organization, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
