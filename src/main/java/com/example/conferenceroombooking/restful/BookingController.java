package com.example.conferenceroombooking.restful;

import com.example.conferenceroombooking.dao.*;
import com.example.conferenceroombooking.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class BookingController {
    @Autowired
    BookingService bookingService;
    @Autowired
    OrganizationRepository organizationRepository;
    @Autowired
    ConferenceRoomRepository conferenceRoomRepository;
    @Autowired
    BookingRepository bookingRepository;

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(path="/booking/{organizationId}/{conferenceRoomId}")
    ResponseEntity<String> addBooking(@Valid @RequestBody Booking booking,
                                      @PathVariable Long organizationId,
                                      @PathVariable Long conferenceRoomId) {

        organizationRepository.findById(organizationId).map(organization -> {
            booking.setOrganization(organization);
            return bookingRepository.save(booking);
        });

        conferenceRoomRepository.findById(conferenceRoomId).map(conferenceRoom -> {
            booking.setConferenceRoom(conferenceRoom);
            return bookingRepository.save(booking);
        });

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(path="/booking")
    Iterable<Booking> getBookings() {
        return bookingService.getBookings();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(path="/booking/{id}")
    Booking getBooking(@PathVariable Long id) {
        return bookingService.getBookingById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/booking/{id}")
    public ResponseEntity<?> updateBooking(@Valid @RequestBody Booking booking, @PathVariable Long id) {
        bookingService.updateBooking(booking, id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
