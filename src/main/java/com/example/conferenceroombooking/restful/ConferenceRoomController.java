package com.example.conferenceroombooking.restful;

import com.example.conferenceroombooking.dao.ConferenceRoomService;
import com.example.conferenceroombooking.model.ConferenceRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ConferenceRoomController {
    @Autowired
    ConferenceRoomService conferenceRoomService;

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(path="/conferenceroom")
    ResponseEntity<String> addConferenceRoom(@Valid @RequestBody ConferenceRoom conferenceRoom) {
        conferenceRoomService.saveConferenceRoom(conferenceRoom);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(path="/conferenceroom")
    Iterable<ConferenceRoom> getConferenceRooms() {
        return conferenceRoomService.getConferenceRooms();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(path="/conferenceroom/{id}")
    ConferenceRoom getConferenceRooms(@PathVariable Long id) {
        return conferenceRoomService.getConferenceRoomById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/conferenceroom/{id}")
    ResponseEntity<String> updateConferenceRoom(@Valid @RequestBody ConferenceRoom conferenceRoom, @PathVariable Long id) {
        conferenceRoomService.updateConferenceRoom(conferenceRoom, id);
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
