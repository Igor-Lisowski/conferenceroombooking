package com.example.conferenceroombooking.dao;

import com.example.conferenceroombooking.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {
    Organization save(Organization persisted);

    @Query(value = "Select * From Organization", nativeQuery = true)
    List<Organization> findOrganizations();

    @Query(value = "Select * From Organization Where id = ?1", nativeQuery = true)
    Organization findOrganizationById(Long id);

    @Query(value = "Delete From Organization Where id = ?1", nativeQuery = true)
    void deleteOrganizationById(Long id);
}
