package com.example.conferenceroombooking.dao;

import com.example.conferenceroombooking.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    Booking save(Booking persisted);

    @Query(value = "Select * From Booking", nativeQuery = true)
    List<Booking> findBookings();

    @Query(value = "Select * From Booking Where id = ?1", nativeQuery = true)
    Booking findBookingById(Long id);

    @Query(value = "Delete From Booking Where id = ?1", nativeQuery = true)
    void deleteBookingById(Long id);
}
