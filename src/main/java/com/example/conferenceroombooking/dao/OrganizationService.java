package com.example.conferenceroombooking.dao;

import com.example.conferenceroombooking.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class OrganizationService {
    @Autowired
    private OrganizationRepository organizationRepository;

    @PostConstruct
    private void init() {}

    public void saveOrganization(Organization organization) {
        this.organizationRepository.save(organization);
    }

    public List<Organization> getOrganizations() {
        return this.organizationRepository.findOrganizations();
    }

    public Organization getOrganizationById(Long id) {
        return this.organizationRepository.findOrganizationById(id);
    }

    public void updateOrganizationById(Organization organization, Long id) {
        organization.setId(id);
        organizationRepository.save(organization);
    }

    public void deleteOrganizationById(Long id) {
        this.organizationRepository.deleteOrganizationById(id);
    }
}
