package com.example.conferenceroombooking.dao;

import com.example.conferenceroombooking.model.ConferenceRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoom, Long> {
    ConferenceRoom save(ConferenceRoom persisted);

    @Query(value = "Select * From ConferenceRoom", nativeQuery = true)
    List<ConferenceRoom> findConferenceRooms();

    @Query(value = "Select * From ConferenceRoom Where id = ?1", nativeQuery = true)
    ConferenceRoom findConferenceRoomById(Long id);

    @Query(value = "Delete From ConferenceRoom  Where id = ?1", nativeQuery = true)
    void deleteConferenceRoomById(Long id);
}
