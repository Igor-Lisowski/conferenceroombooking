package com.example.conferenceroombooking.dao;

import com.example.conferenceroombooking.model.ConferenceRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class ConferenceRoomService {
    @Autowired
    private ConferenceRoomRepository conferenceRoomRepository;

    @PostConstruct
    private void init() {}

    public void saveConferenceRoom(ConferenceRoom conferenceRoom) {
        this.conferenceRoomRepository.save(conferenceRoom);
    }

    public List<ConferenceRoom> getConferenceRooms() {
        return this.conferenceRoomRepository.findConferenceRooms();
    }

    public ConferenceRoom getConferenceRoomById(Long id) {
        return this.conferenceRoomRepository.findConferenceRoomById(id);
    }

    public void updateConferenceRoom(ConferenceRoom conferenceRoom, Long id) {
        conferenceRoom.setId(id);
        conferenceRoomRepository.save(conferenceRoom);
    }

    public void deleteConferenceRoomById(Long id) {
        this.conferenceRoomRepository.deleteConferenceRoomById(id);
    }
}
