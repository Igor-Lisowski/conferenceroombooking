package com.example.conferenceroombooking.dao;

import com.example.conferenceroombooking.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class BookingService {
    @Autowired
    private BookingRepository bookingRepository;

    @PostConstruct
    private void init() {}

    public void saveBooking(Booking booking) {
        this.bookingRepository.save(booking);
    }

    public List<Booking> getBookings() {
        return this.bookingRepository.findBookings();
    }

    public Booking getBookingById(Long id) {
        return this.bookingRepository.findBookingById(id);
    }

    public void updateBooking(Booking booking, Long id) {
        booking.setId(id);
        this.bookingRepository.save(booking);
    }

    public void deleteBookingById(Long id) {
        this.bookingRepository.deleteBookingById(id);
    }
}
