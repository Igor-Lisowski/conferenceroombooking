import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import Cookies from 'js-cookie';
import { Route, Link, BrowserRouter as Router} from 'react-router-dom';

import Register from './Register';
import LogIn from './LogIn';
import routing from './index';

export default class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
    
    this.toggle = this.toggle.bind(this);
    this.state = {
        rooms: null,
        isOpen: false,
        navCollapsed: true,
        showNavbar: false
    };
  }

  toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
  }

  componentDidMount() {
    this.renderOptions();
  }

  renderOptions = async() => {
    const requestOptions = {
      method: 'GET',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer'
    };

  fetch('http://localhost:8090/conferenceroom', requestOptions)
    .then(response => {
      return response.text();
    })
    .then(responseBodyAsText => {
      try {
        if (responseBodyAsText === '') {
          return JSON.stringify({});
        }
          const bodyAsJson = JSON.parse(responseBodyAsText);
          return bodyAsJson;
      } catch (e) {
          Promise.reject({body:responseBodyAsText, type:'unparsable'});
      }
    })
    .then(json => {
      var elements = [];
      for (var i = 0; i < json.length; i++) {
        let link = "/conferenceroom/" + json[i]['id'];
        elements.push(<DropdownItem href={link}>{json[i]['name']}</DropdownItem>);
      }
      this.setState({
        rooms: elements
      });
    })
    .catch( err => {
      if (false === err instanceof Error &&  err.type && err.type === 'unparsable') {
          console.log('error:');
          console.log(err.body);
          return;
      }
      throw err;
    });
  }

  render() {
    return (
      <div>
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Conference Rooms
              </DropdownToggle>
              <DropdownMenu right>
                {this.state.rooms}
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <UserOptions/>
        </Collapse>
      </Navbar>
    </div>
    );
  }
}

function UserOptions() {
  if (!Cookies.get('access_token')) {
    return (
      <Nav navbar>
      <NavItem>
          <NavLink href="/login">Log In</NavLink>
      </NavItem>
      <NavItem>
          <NavLink href="/register">Register</NavLink>
      </NavItem>
      </Nav>
      );
  } else {
    return (
      <Nav navbar>
      <NavItem>
          <NavLink href="/logout">Logout</NavLink>
      </NavItem>
      </Nav>
      );
  }
}

function getRooms() {
  const requestOptions = {
      method: 'GET',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer'
  };

  var elements = fetch('http://localhost:8090/conferenceroom', requestOptions)
    .then(response => {
      return response.text();
    })
    .then(responseBodyAsText => {
      try {
        if (responseBodyAsText === '') {
          return JSON.stringify({});
        }
          const bodyAsJson = JSON.parse(responseBodyAsText);
          return bodyAsJson;
      } catch (e) {
          Promise.reject({body:responseBodyAsText, type:'unparsable'});
      }
    })
    .then(json => {
      var elements = [];
      for (var i = 0; i < json.length; i++) {
        elements.push(<DropdownItem>{json[i]['name']}</DropdownItem>);
      }
      return elements;
    })
    .catch( err => {
      if (false === err instanceof Error &&  err.type && err.type === 'unparsable') {
          console.log('error:');
          console.log(err.body);
          return;
      }
      throw err;
    });
    return elements;
}

export { NavigationBar };