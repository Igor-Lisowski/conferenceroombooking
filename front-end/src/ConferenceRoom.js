import React from 'react';

import Cookies from 'js-cookie';
import NavigationBar from './NavigationBar';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import DatePicker from 'reactstrap-date-picker';
import TimePicker from 'react-bootstrap-time-picker';

import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import bootstrapPlugin from '@fullcalendar/bootstrap';


export default class ConferenceRoom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: new Date().toISOString(),
      start: '8:00',
      end: '8:30',
      conferenceRoomId: this.props.match.params.id,
      events: null
    }
    this.handleStartChange = this.handleStartChange.bind(this);
    this.handleEndChange = this.handleEndChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  render() {
    return (
      <div>
        <NavigationBar/>
        <Form onSubmit={this.submitForm}>
          <h2 className="text-center">Welcome</h2>
          <FormGroup>
            <Label>Date</Label>
            <DatePicker id="date" type="date" name="date" placeholder="Date" value={this.state.value} onChange={(v,f) => this.handleChange(v, f)}></DatePicker>
          </FormGroup>
          <FormGroup>
            <Label>Start</Label>
            <TimePicker name="start" onChange={this.handleStartChange} value={this.state.start} value={this.state.start} start="8:00" end="19:30" step={30}/>
          </FormGroup>
          <FormGroup>
            <Label>End</Label>
            <TimePicker name="end" onChange={this.handleEndChange} value={this.state.end} value={this.state.end} start="8:30" end="20:00" step={30}/>
          </FormGroup>
          <Button className="btn-lg btn-dark btn-block">Submit</Button>
        </Form>
        <FullCalendar
          plugins={[ timeGridPlugin ]}
          events={this.state.events}
        />
      </div>
    );
  }

  handleChange(value, formattedValue) {
    this.setState({
      value: value, // ISO String, ex: "2016-11-19T12:00:00.000Z"
      formattedValue: formattedValue // Formatted String, ex: "11/19/2016"
    })
  }

  handleStartChange(start) {
    this.setState({start: start});
  }

  handleEndChange(end) {
    this.setState({end: end});
  }

  onChange = time => this.setState({ time });

  componentDidMount() {
    this.getEvents();
    var el = document.getElementById('calendar');
  }

  async submitForm(event) {
    event.preventDefault();
    const formData = new FormData(event.target);

    const requestOptions = {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      body: JSON.stringify({
        start: this.convertTime(formData.get('date'), formData.get('start')),
        finish: this.convertTime(formData.get('date'),formData.get('end'))
      })
    };

    if (!!Cookies.get('access_token')) {
      fetch('http://localhost:8090/booking/' + Cookies.getJSON('access_token')['id'] + '/' + this.state.conferenceRoomId, requestOptions)
        .then(response => {
          return response.text();
        })
        .then(responseBodyAsText => {
          try {
            if (responseBodyAsText === '') {
              return JSON.stringify({});
            }
            const bodyAsJson = JSON.parse(responseBodyAsText);
            return bodyAsJson;
          } catch (e) {
            alert('Something went wrong');
            Promise.reject({body:responseBodyAsText, type:'unparsable'});
          }
        })
    } else {
      alert('Log in to make booking');
    }
  }

  convertTime(date, time) {
    let dateSubString = date.substring(0, date.indexOf('T') + 1);
    var timeSubString;
    if (time % 3600 == 0) {
      timeSubString = (time / 3600) + ":00:00"; 
    } else if (time % 1800 == 0) {
      timeSubString = parseInt(time / 3600) + ":30:00";
    }

    console.log(dateSubString + timeSubString);
    return dateSubString + timeSubString;
  }

  getEvents = async() => {
    const requestOptions = {
      method: 'GET',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer'
    };

    fetch('http://localhost:8090/conferenceroom/' + this.state.conferenceRoomId, requestOptions)
      .then(response => {
        return response.text();
      })
      .then(responseBodyAsText => {
      try {
        if (responseBodyAsText === '') {
          return JSON.stringify({});
        }
          const bodyAsJson = JSON.parse(responseBodyAsText);
          return bodyAsJson;
        } catch (e) {
            Promise.reject({body:responseBodyAsText, type:'unparsable'});
        }
      })
      .then(json => {
        var elements = [];
        console.log(json);
        for (var i = 0; i < json['bookings'].length; i++) {
          elements.push({title: json['bookings'][i]['organization']['name'], start: json['bookings'][i]['start'], end: json['bookings'][i]['finish']})
        }
        this.setState({
          events: elements
        });
      })
      .catch(err => {
        if (false === err instanceof Error &&  err.type && err.type === 'unparsable') {
            console.log('error:');
            console.log(err.body);
            return;
        }
        throw err;
      });
  }
}
