import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

import Cookies from 'js-cookie';
import NavigationBar from './NavigationBar';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default class LogOut extends React.Component {
	render() {
		return (
		    <div>
          <SignOut/>
			    <NavigationBar/>
			    <h2 className="text-center">Thank you for using our product!</h2>
		    </div>
		);
	}
}

function SignOut() {
  Cookies.remove('access_token');
  console.log('token removed');
  return null;
}

export { LogOut }; 