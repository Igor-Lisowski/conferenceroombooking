import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Route, Link, BrowserRouter as Router} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.css';

import App from './App';
import ConferenceRoom from './ConferenceRoom';
import LogIn from './LogIn';
import LogOut from './LogOut';
import Register from './Register';


const routing = (
	<Router>
		<Route exact path="/" component={App} />
		<Route path="/login" component={LogIn} />
		<Route path="/logout" component={LogOut} />
		<Route path="/register" component={Register} />
		<Route path="/conferenceroom/:id" component={ConferenceRoom} />
	</Router>
	)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
