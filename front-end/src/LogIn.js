import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

import Cookies from 'js-cookie';
import NavigationBar from './NavigationBar';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default class LogIn extends React.Component {
	constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

	render() {
		return (
			    <div>
				    <NavigationBar/>
				    <Form onSubmit={this.submitForm} className="login-form">
				      <h2 className="text-center">Welcome</h2>
				      <FormGroup>
				        <Label>Name</Label>
				        <Input type="text" name="Name" placeholder="Name"></Input>
				      </FormGroup>
				      <FormGroup>
				        <Label>Password</Label>
				        <Input type="password" name="Password" placeholder="Password"></Input>
				      </FormGroup>
				      <Button className="btn-lg btn-dark btn-block">Log In</Button>
				    </Form>
			    </div>
		);
	}

	submitForm(event) {
		event.preventDefault();
		const formData = new FormData(event.target);

		const requestOptions = {
			method: 'GET',
			mode: 'cors',
			cache: 'no-cache',
			credentials: 'same-origin',
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Credentials': true
			},
			redirect: 'follow',
			referrerPolicy: 'no-referrer'
		};

		fetch('http://localhost:8090/organization', requestOptions)
			.then(response => {
		        	return response.text();
		    	})
		    .then(responseBodyAsText => {
		        try {
		        	if (responseBodyAsText === '') {
		        		return JSON.stringify({});
		        	}
		            const bodyAsJson = JSON.parse(responseBodyAsText);
		            return bodyAsJson;
		        } catch (e) {
		            Promise.reject({body:responseBodyAsText, type:'unparsable'});
		        }
		    })
		    .then(json => {
		          	for (var i = 0; i < json.length; i++) {
		          		if (json[i]['name'] == formData.get('Name') && json[i]['password'] == formData.get('Password')) {
		          			alert('Login successful');

		          			let todayDate = new Date();
		          			let access_token = {
		          				'id': json[i]['id'],
		          				'name': json[i]['name']
		          			}

		          			Cookies.set('access_token', access_token, { expires: new Date(todayDate.setHours(todayDate.getHours() + 1)) });

		          			this.props.history.push('/');
		          			return;
		          		}
		          	}
		          	alert('Login unsuccessful');
		    })
		    .catch( err => {
		        if (false === err instanceof Error &&  err.type && err.type === 'unparsable') {
		            console.log('error:');
		            console.log(err.body);
		            return;
		        }
		        throw err;
		    });
	}
}