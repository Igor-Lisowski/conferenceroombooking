import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

import NavigationBar from './NavigationBar';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default class Register extends React.Component {
	constructor(props) {
        super(props);

        this.state = {
            errorMessage: null
        };
    }

	render() {
		return (
		    <div>
			    <NavigationBar/>
			    <Form onSubmit={this.submitForm} className="login-form">
			      <h2 className="text-center">Welcome</h2>
			      <FormGroup>
			        <Label>Name</Label>
			        <Input type="text" name="Name" placeholder="Name"></Input>
			      </FormGroup>
			      <FormGroup>
			        <Label>Password</Label>
			        <Input type="password" name="Password" placeholder="Password"></Input>
			      </FormGroup>
			      <Button className="btn-lg btn-dark btn-block">Register</Button>
			    </Form>
		    </div>
		);
	}

	async submitForm(event) {
		event.preventDefault();
		const formData = new FormData(event.target);

		const requestOptions = {
			method: 'POST',
			mode: 'cors',
			cache: 'no-cache',
			credentials: 'same-origin',
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Credentials': true
			},
			redirect: 'follow',
			referrerPolicy: 'no-referrer',
			body: JSON.stringify({
				name: formData.get('Name'), 
				password: formData.get('Password')
			})
		};

		fetch('http://localhost:8090/organization', requestOptions)
			.then(response => {
	        	return response.text();
	    	})
		    .then(responseBodyAsText => {
		        try {
		        	console.log(typeof responseBodyAsText);
		        	if (responseBodyAsText === '') {
		        		return JSON.stringify({});
		        	}
		        	console.log(responseBodyAsText);
		            const bodyAsJson = JSON.parse(responseBodyAsText);
		            return bodyAsJson;
		        } catch (e) {
		            Promise.reject({body:responseBodyAsText, type:'unparsable'});
		        }
		    })
		    .then( json => {
		            console.log(json);
		          	alert('Registration successful');
		    })
		    .catch( err => {
		        if (false === err instanceof Error &&  err.type && err.type === 'unparsable') {
		            // this.props.dispatch(displayTheError(err.body))
		            // this.props.dispatch(console.log(err.body));
		            console.log('error:');
		            console.log(err.body);
		            return;
		        }
		        throw err;
		    });
	}
}

export { Register }; 