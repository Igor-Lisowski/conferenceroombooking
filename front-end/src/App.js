import React from 'react';
import './App.css';

import NavigationBar from './NavigationBar';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import DatePicker from 'reactstrap-date-picker';
import TimePicker from 'react-bootstrap-time-picker';

import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import bootstrapPlugin from '@fullcalendar/bootstrap';


export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <NavigationBar/>
        <h1>Welcome</h1>
      </div>
    );
  }
}
